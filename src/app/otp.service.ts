import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class OtpService {

  randomOtp: any;
  mobile: any;
  constructor(private http: HttpClient) { }

  url = 'http://lab.thinkoverit.com/api';

  getOTP(body: any) {
    const headers = {};
    //Getting cors error in api 
    // return this.http.post(this.url + '/getOTO.php', body, { headers });
    this.mobile = body.mobile;
    this.randomOtp = Math.floor(1000 + Math.random() * 9000);
    return of({ otp: this.randomOtp, status: true });
  }

  verifyOtp(body: any) {
    const headers = {};
    // return this.http.post(this.url + '/verifyOTP.php', body, { headers })
    if (body.otp == this.randomOtp && body.mobile === this.mobile) {
      return of(true)
    } else {
      return of(false)
    }
  }
}
