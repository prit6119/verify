import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { OtpService } from '../otp.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {

  constructor(private _fb: FormBuilder, private _router: Router, private _otpService: OtpService) { }
  unableForm = false;
  clickCount = 0;
  clickCountError: boolean = false;
  mobileNumer: any;

  verifyForm = this._fb.group({
    city: ['', [Validators.required]],
    panNumber: ['', [Validators.required, Validators.pattern("^[A-Za-z]{5}[0-9]{4}[A-Za-z]$"), Validators.maxLength(10)]],
    fullName: ['', [Validators.required, Validators.maxLength(140)]],
    email: ['', [Validators.required, Validators.email, Validators.maxLength(255), Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
    mobile: ['', [Validators.required, Validators.maxLength(10), Validators.pattern("[0-9]{10}")]],

  });

  verifyOTP = this._fb.group({
    mobile: [this.verifyForm.value.mobile],
    otp: ['', [Validators.maxLength(4), Validators.required]]
  });

  visible: boolean = false;
  waitSeconds: boolean = true;
  success: boolean = false;
  ngOnInit(): void {
  }

  get f() {
    return this.verifyForm.controls;
  }

  getOtp() {
    let body = {
      city: this.verifyForm.value.city,
      panNumber: this.verifyForm.value.panNumber,
      fullName: this.verifyForm.value.fullName,
      email: this.verifyForm.value.email,
      mobile: this.verifyForm.value.mobile,
    }
    this.mobileNumer = this.verifyForm.value.mobile;

    this._otpService.getOTP(body).subscribe((data: any) => {
      if (data.status) {
        this.visible = true;
        this.verifyOTP.setValue({ otp: data.otp, mobile: this.mobileNumer });
        setTimeout(() => { this.waitSeconds = false }, 180000);
      }
    })
  }

  verify() {
    this._otpService.verifyOtp(this.verifyOTP.value).subscribe((data: any) => {
      if (data) {
        this.success = true;
      } else {
        alert('OTP is wrong.')
      }
    })
  }

  resendOTP() {
    this.clickCount = ++this.clickCount;
    if (this.clickCount > 3) {
      this.clickCountError = true;
    } else {
      this.getOtp();
    }
  }
}

